﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AirportGame
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowVM client;
        DispatcherTimer _timer;
        TimeSpan _time;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowVM();
            client = (MainWindowVM)DataContext;
            client.GL.Bankruptcy += Losing;
        }

        public void Losing()
        {
            if (MessageBox.Show("Вы проиграли :(\nХотите начать новую игру?", "Проигрыш", 
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                //client = new GameLogic();
                DataContext = new MainWindowVM();
                client = (MainWindowVM)DataContext;
            }
            else
            {
                Close();
            }
        }
    }
}
