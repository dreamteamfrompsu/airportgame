﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using AirportGame.VM;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AirportGame
{
    class MainWindowVM : ViewModelBase
    {
        public GameLogic GL = new GameLogic();
        
        private RelayCommand _x1Command;
        private RelayCommand _x50Command;
        private RelayCommand _x500Command;

        private List<PlaneVM> _planes
            = new List<PlaneVM>();

        private List<PlaneVM> _planesShop
            = new List<PlaneVM>();

        private List<FlightVM> _flights
           = new List<FlightVM>();

        private List<MyFlightVM> _myflights
           = new List<MyFlightVM>();

  

        private List<Place> _places = new List<Place>();

        public MainWindowVM()
        {
            
            PopulatePlanes();
            Refresh();
            
            GL.RealTime.Tick += new EventHandler(RefreshTime);
        }

        /// <summary>
        ///   Список самолётов.
        /// </summary>
        public ReadOnlyCollection<PlaneVM> Planes
            => new ReadOnlyCollection<PlaneVM>(_planes);

        public ReadOnlyCollection<PlaneVM> PlanesShop
            => new ReadOnlyCollection<PlaneVM>(_planesShop);

        public ReadOnlyCollection<FlightVM> Flights
            => new ReadOnlyCollection<FlightVM>(_flights);

        public ReadOnlyCollection<MyFlightVM> MyFlights
           => new ReadOnlyCollection<MyFlightVM>(_myflights);  

        private void Refresh()
        {
            PopulatePlanes();
            PopulateFlights();

            RaisePropertyChanged(nameof(Flights));
            RaisePropertyChanged(nameof(MyFlights));
            RaisePropertyChanged(nameof(Planes));
            RaisePropertyChanged(nameof(PlanesShop));
            RaisePropertyChanged(nameof(UserTime));
            RaisePropertyChanged(nameof(Money));
        }

        private void PopulatePlanes()
        {
            _planes.Clear();
            _planes.AddRange(GL.Airport.Select(p => new PlaneVM(p, this)));
            _planesShop.Clear();
            _planesShop.AddRange(GL.PlanesShop.Select(p => new PlaneVM(p, this)));
        }

        private void PopulateFlights()
        {
            _flights.Clear();
            _myflights.Clear();
            _flights.AddRange(GL.AvailableFlights.Select(p=> new FlightVM(p, this)));
            _myflights.AddRange(GL.TakenFlights.Select(p => new MyFlightVM(p, _planes, this)));
        }

        internal void AddFlight(FlightVM flightVM)
        {
            GL.TakeFlight(flightVM.AbstractFlight);
            Refresh();
        }

        internal void DeleteFlight(MyFlightVM flightVM)
        {
            GL.DeleteFlight(flightVM.AbstractFlight);
            Refresh();
        }

        internal void BuyPlane(PlaneVM plane)
        {
            GL.BuyPlane(plane.Plane);
            Refresh();
        }

        internal void SellPlane(PlaneVM plane)
        {
            GL.SellPlane(plane.Plane);
            Refresh();
        }

        private void RefreshTime(Object sender, EventArgs e)
        {
            RaisePropertyChanged(nameof(UserTime));
            RaisePropertyChanged(nameof(Money));
            int lastCountFlight = MyFlights.Count;
            if (GL.UserTime.Hour == 0 && GL.UserTime.Minute == 0)
            {
                PopulateFlights();
                RaisePropertyChanged(nameof(Flights));
            }
            foreach (var flight in MyFlights)
            {
                if (flight.AbstractFlight.DueDate == GL.UserTime)
                {
                    PopulateFlights();
                    RaisePropertyChanged(nameof(Planes));
                    RaisePropertyChanged(nameof(MyFlights));
                    break;
                }
            }
            if (GL.TakenFlights.Count != lastCountFlight)
            {
                PopulateFlights();
                RaisePropertyChanged(nameof(Planes));
                RaisePropertyChanged(nameof(MyFlights));
            }

        }
        
        public string UserTime
        {
            get { return GL.UserTime.ToString("dd/MM/yyyy HH:mm"); }
        }

        public string Money
        {
            get {return String.Format("{0} ₽", GL.Money);}
        }
        
        // Комманды
        
        public ICommand X1Command
            => _x1Command ?? (_x1Command = new RelayCommand(timeX1));
        public ICommand X50Command
            => _x50Command ?? (_x50Command = new RelayCommand(timeX50));
        public ICommand X500Command
            => _x500Command ?? (_x500Command = new RelayCommand(timeX500));

        private void timeX1()
        {
            GL.RealTime.Interval = 1000;
        }
        
        private void timeX50()
        {
            GL.RealTime.Interval = 20;
        }
        
        private void timeX500()
        {
            GL.RealTime.Interval = 1;
        }
    }
}