﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;


namespace AirportGame.VM
{
    class PlaneVM : ViewModelBase
    {
        private RelayCommand _buyCommand;
        private RelayCommand _sellCommand;

        public PlaneVM(Plane plane, MainWindowVM parentVM)
        {
            Plane = plane;
            ParentVM = parentVM;
        }

        public MainWindowVM ParentVM
        {
            get;
            private set;
        }

        public Plane Plane
        {
            get;
            private set;
        }

        public string Name
        {
            get
            {
                return Plane.Name;
            }
        }

        public string Cappacity
        {
            get
            {
                return Plane.PassengerCapacity.ToString() + " пасс. \n"
                    + Plane.CarryingCapacity.ToString() + " тонн";
            }
        }

        public string MaxSpeed
        {
            get
            {
                return Plane.MaxSpeed.ToString() + "км/ч";
            }
        }

        public string MaxDist
        {
            get
            {
                return Plane.MaxFlightDistance.ToString() + "км";
            }
        }

        public string Place
        {
            get
            {
                return Plane.CurentPlace?.Name;
            }
        }

        public string Price
        {
            get
            {
                return Plane.ServiceCost.ToString() + "руб/км";
            }
        }

        public string BuyText
        {
            get
            {
                if (Plane.Contract.Type == EContractType.Purchase)
                {
                    return (Plane.Contract.InitFee).ToString() + " руб";
                }
                else if (Plane.Contract.Type == EContractType.Lease)
                {
                    return (Plane.Contract.InitFee).ToString() + " руб + " + Plane.Contract.DailyFee.ToString() + " руб/день";
                }
                else
                {
                    return (Plane.Contract.DailyFee.ToString() + " руб/день");
                }
            }
        }

        public string SellText
        {
            get
            {
                if (Plane.Contract.Type == EContractType.Purchase)
                {
                    return (Plane.Cost * PlaneContract.SellCoef).ToString() + " руб";
                }
                else
                {
                    return Plane.Contract.DailyFee.ToString() + " руб/день";
                }
            }
        }

        public ICommand BuyCommand
            => _buyCommand ?? (_buyCommand = new RelayCommand(Buy));

        public ICommand SellCommand
            => _sellCommand ?? (_sellCommand = new RelayCommand(Sell));

        public void Buy()
        {
            ParentVM.BuyPlane(this);
        }

        public void Sell()
        {
            ParentVM.SellPlane(this);
        }
    }
}