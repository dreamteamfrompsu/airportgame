﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AirportGame.VM;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AirportGame
{
    class MyFlightVM : ViewModelBase
    {
        private List<Plane> _planebox
           = new List<Plane>();

        private RelayCommand _deleteCommand;

        public MyFlightVM(AbstractFlight abstractFlight, List<PlaneVM> planes, MainWindowVM parentVM)
        {
            AbstractFlight = abstractFlight;
            ParentVM = parentVM;
            Planes = planes;

            PopulatePlaneBox();
        }

        public List<PlaneVM> Planes
        {
            get;
            set;
        }

        public AbstractFlight AbstractFlight
        {
            get;
            set;
        }

        public MainWindowVM ParentVM
        {
            get;
            private set;
        }

        public ICommand DeleteCommand
            => _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete));


        public string FromToWhere
        {
            get
            {
                return AbstractFlight.From.Name + "->" + AbstractFlight.Where.Name;
            }
        }

        public string Type
        {
            get
            {
                if (AbstractFlight.FlightType == EFlightType.Cargo && AbstractFlight.FrequencyType == EFrequencyType.OneTime)
                    return "Грузовой\n" + AbstractFlight.PlannedFlightDate.ToString("dd/MM/yyyy HH:mm");
                if (AbstractFlight.FlightType == EFlightType.Passenger && AbstractFlight.FrequencyType == EFrequencyType.OneTime)
                    return "Разовый\n" + AbstractFlight.PlannedFlightDate.ToString("dd/MM/yyyy HH:mm");
                if (AbstractFlight.FlightType == EFlightType.Passenger && AbstractFlight.FrequencyType == EFrequencyType.Regular)
                    return "Каждые " + ((PassengerFlight)AbstractFlight).Frequency / (12 * 60) + " дней";
                else return "";
            }
        }

        public string MaxWeightAndDist
        {
            get
            {
                string maxweight = "";
                if (AbstractFlight is PassengerFlight)
                    maxweight = ((PassengerFlight)AbstractFlight).Passengers.ToString() + " пасс";
                if (AbstractFlight is CargoFlight)
                    maxweight = ((CargoFlight)AbstractFlight).CargoWeight.ToString() + " тонны";
                return maxweight + "\n" + AbstractFlight.Distance.ToString() + " км";
            }
        }

        public string IncomeAndPenalty
        {
            get
            {
                int income = AbstractFlight.Income;
                if (AbstractFlight is PassengerFlight)
                    return income.ToString() + " руб/пассажир \n" + AbstractFlight.Penalty.ToString() + " руб";
                if (AbstractFlight is CargoFlight)
                    return income + " руб" + "\n" + AbstractFlight.Penalty.ToString() + " руб";
                else return "";

            }
        }

        public string PlaneBoxName
        {
            get
            {
                return AbstractFlight.AssignedPlane?.Name;
            }
            set
            {
                AbstractFlight.AssignPlane(PlanesBox.Where(e => e.Name == value).FirstOrDefault());
                RaisePropertyChanged(nameof(PlaneBoxName));
            }
        }

        public IList<Plane> PlanesBox => _planebox;

        private void PopulatePlaneBox()
        {
            _planebox.Clear();
            foreach (var plane in Planes)
                if (AbstractFlight.IsAssignable(plane.Plane))
                    _planebox.Add(plane.Plane);
        }

        public DateTime FirstDate
        {
            get
            {

                return AbstractFlight.PlannedFlightDate == DateTime.MaxValue ? DateTime.Now : AbstractFlight.PlannedFlightDate;
               
            }
            set
            {

                AbstractFlight.PlannedFlightDate = value;

                RaisePropertyChanged(nameof(FirstDate));
            }
        }

        public DateTime MaximumTime
        {
            get
            {
                DateTime temp = AbstractFlight.PlannedFlightDate.
                    AddMinutes(-AbstractFlight.PlannedFlightDate.Minute - 1);
                temp = temp.AddHours(-AbstractFlight.PlannedFlightDate.Hour);
                temp = temp.AddDays(1);
                return AbstractFlight.FrequencyType == EFrequencyType.OneTime ? 
                    temp : DateTime.MaxValue;
            }
        }
        public DateTime MinimumTime
        {
            get
            {
                DateTime temp = AbstractFlight.PlannedFlightDate.
                    AddMinutes(-AbstractFlight.PlannedFlightDate.Minute);
                temp = temp.AddHours(-AbstractFlight.PlannedFlightDate.Hour);
                return AbstractFlight.FrequencyType == EFrequencyType.OneTime ?
                    temp : DateTime.MinValue;
            }
        }

        public bool OneTimeIsEnabled => AbstractFlight.FrequencyType == EFrequencyType.OneTime? false : true;
        

        public string DueTime
        {
            get
            {
                return AbstractFlight.DueDate.ToString("dd/MM/yyyy HH:mm");
            }
        }

        public DateTime PlannedFlightDate
        {
            get
            {
                return AbstractFlight.PlannedFlightDate;
            }
        }

        private void Delete()
        {
            ParentVM.DeleteFlight(this);
        }
    }
}