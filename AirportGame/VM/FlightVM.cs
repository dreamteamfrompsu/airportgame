﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AirportGame
{
    class FlightVM : ViewModelBase
    {
        private RelayCommand _addCommand;

        public FlightVM(AbstractFlight abstractFlight, MainWindowVM parentVM)
        {
            AbstractFlight = abstractFlight;
            ParentVM = parentVM;
        }

        public AbstractFlight AbstractFlight
        {
            get;
            private set;
        }

        public MainWindowVM ParentVM
        {
            get;
            private set;
        }

    
        public ICommand AddCommand
            => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public string FromToWhere
        {
            get
            {
                return AbstractFlight.From.Name + "->" + AbstractFlight.Where.Name;
            }
        }

        public string Type
        {
            get
            {
                if (AbstractFlight.FlightType == EFlightType.Cargo && AbstractFlight.FrequencyType == EFrequencyType.OneTime)
                    return "Грузовой ";
                if (AbstractFlight.FlightType == EFlightType.Passenger && AbstractFlight.FrequencyType == EFrequencyType.OneTime)
                    return "Разовый";
                if (AbstractFlight.FlightType == EFlightType.Passenger && AbstractFlight.FrequencyType == EFrequencyType.Regular)
                    return "Каждые " + ((PassengerFlight)AbstractFlight).Frequency / (12 * 60) + " дней";
                else return "";
            }
        }

        public string MaxWeightAndDist
        {
            get
            {
                string maxweight = "";
                if (AbstractFlight is PassengerFlight)
                    maxweight = ((PassengerFlight)AbstractFlight).Passengers.ToString() + " пасс";
                if (AbstractFlight is CargoFlight)
                    maxweight = ((CargoFlight)AbstractFlight).CargoWeight.ToString() + " тонны";
                return maxweight + "\n" + AbstractFlight.Distance.ToString() + " км";
            }
        }

        public string IncomeAndPenalty
        {
            get
            {
                int income = AbstractFlight.Income;
                if (AbstractFlight is PassengerFlight)
                    return income.ToString() + " руб/пассажир \n" + AbstractFlight.Penalty.ToString() + " руб";
                if (AbstractFlight is CargoFlight)
                    return income + " руб" + "\n" + AbstractFlight.Penalty.ToString() + " руб";
                else return "";

            }
        }

        public string DueTime
        {
            get
            {
                return AbstractFlight.DueDate.ToString("dd/MM/yyyy HH:mm");
            }
        }

        private void Add()
        {
            ParentVM.AddFlight(this);
        }
    }
}