﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AirportGame.VM;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AirportGame
{
    class PlanesShop : ViewModelBase
    {

        public PlanesShop(List<PlaneVM> planesShop, MainWindowVM parentVM)
        {
            ParentVM = parentVM;
            Shop = planesShop;
        }

        public List<PlaneVM> Shop
        {
            get;
            set;
        }

        public MainWindowVM ParentVM
        {
            get;
            private set;
        }

    }
}