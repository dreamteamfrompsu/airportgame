using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace AirportGame
{
    public enum EFrequencyType
    {
        OneTime = 0,
        Regular
    }

    public enum EFlightType
    {
        Cargo = 0,
        Passenger
    }
    
    public abstract class AbstractFlight
    {
        protected static Random rnd = new Random();
        
        //Measured in rubles
        public static int MinPenalty = 50000;
        public static int MaxPenalty = 300000;
        
        public Place From { get; protected set; }
        public Place Where { get; protected set; }
        
        public EFrequencyType FrequencyType { get; protected set; }
        public EFlightType FlightType { get; protected set; }
        
        public int Penalty { get; protected set; }
        public int Distance { get; protected set; }
        // Adds one time for CargoFlight and multiply by number of passengers for PassengerFlight
        public int Income { get; set; }
        
        public DateTime DueDate { get; protected set; } //Дата окончания действия рейса
        public DateTime PlannedFlightDate { get; set; } //Дата запланированного рейса
        public DateTime DeadlineDate { get; set; } //Дата, до которой должен быть совершет рейс
        
        public Plane AssignedPlane { get; protected set; }

        protected AbstractFlight(Place from, Place where, EFrequencyType frequencyType, 
            EFlightType flightType, int penalty, DateTime dueDate, 
            DateTime plannedFlightDate, int income)
        {
            From = from;
            Where = where;
            FrequencyType = frequencyType;
            FlightType = flightType;
            Penalty = penalty;
            Distance = Place.CalcDistance(from, where);
            DueDate = dueDate;
            AssignedPlane = null;
            PlannedFlightDate = plannedFlightDate;
            Income = income;
        }
        
        protected AbstractFlight(List<Place> placesList, EFrequencyType frequencyType, EFlightType flightType, 
            DateTime currentTime)
        {
            //Назначаем случайное место
            From = placesList[rnd.Next(placesList.Count)];
            
            do
            {
                Where = placesList[rnd.Next(placesList.Count)];
            } while (From == Where);
            
            //Инициализируем поля
            FlightType = flightType;
            FrequencyType = frequencyType;
            
            Distance = Place.CalcDistance(From, Where);
            Penalty = rnd.Next(MaxPenalty - MinPenalty) + MinPenalty;

            if (FrequencyType == EFrequencyType.OneTime)
                PlannedFlightDate = currentTime.AddDays(rnd.Next(1, 5));
            else
                PlannedFlightDate = DateTime.MaxValue; //Будет назначено при выборе даты вылета
            DeadlineDate = DateTime.MaxValue; //Будет назначено при выборе рейса
            DueDate = currentTime.AddMinutes(7 * 24 * 60);
            AssignedPlane = null;
        }
        
        public void AssignPlane(Plane plane) 
        {
            if (IsAssignable(plane))
                AssignedPlane = plane;
        }
        
        public abstract bool IsAssignable(Plane plane);
        public abstract void OnPlaneFlight(GameLogic gl);
        public abstract void UpdateMoneyAfterFlight(GameLogic gameLogic);
    }

    public class CargoFlight : AbstractFlight
    {
        public static int MinCargoWeight = 12;
        public static int MaxCargoWeight = 220;
        
        public double CargoWeight { get; }

        public CargoFlight(Place from, Place where, int penalty, DateTime dueDate, 
            double cargoWeight, DateTime plannedFlightDate, int income) : 
            base(from, where, EFrequencyType.OneTime, EFlightType.Cargo, 
                penalty, dueDate, plannedFlightDate, income)
        {
            CargoWeight = cargoWeight;
            PlannedFlightDate = DateTime.Now;
        }
        
        public CargoFlight(List<Place> placesList, DateTime currentTime): 
            base(placesList, EFrequencyType.OneTime, EFlightType.Cargo, currentTime)
        {
            CargoWeight = rnd.Next(MaxCargoWeight - MinCargoWeight) + MinCargoWeight;
            Income = (int)(5.0 * Distance * GameLogic.FuelLiterCost * rnd.Next(7, 30) / 10.0);
        }

        public override bool IsAssignable(Plane plane)
        {
            return plane.MaxFlightDistance >= Distance && plane.CarryingCapacity >= CargoWeight;
        }

        public override void OnPlaneFlight(GameLogic gl)
        {
            if (AssignedPlane == null)
                return;
            gl.TakenFlights.Remove(this);
        }

        public override void UpdateMoneyAfterFlight(GameLogic gameLogic) 
        {
            if (AssignedPlane == null)
            {
                return;
                //throw new Exception("No assigned plane!");
            }
            int income = Income;
            int expenditure = Convert.ToInt32(Place.CalcDistance(From, Where) * 
                GameLogic.FuelLiterCost * AssignedPlane.FuelPerKm);
            gameLogic.AddMoney(income - expenditure);
        }
    }

    public class PassengerFlight : AbstractFlight
    {
        public static int MinPassengers = 80;
        public static int MaxPassengers = 435;
        //Measured in days
        public static int MinFrequency = 1;
        public static int MaxFrequency = 14;
        
        public int Passengers { get; }
        public int Frequency { get; set; }

        public PassengerFlight(Place from, Place where, EFrequencyType frequencyType, 
            int penalty, DateTime dueDate, int passengers, int frequency, 
            DateTime plannedFlightDate, int income) : 
            base(from, where, frequencyType, EFlightType.Passenger, penalty, dueDate, 
                plannedFlightDate, income)
        {
            Passengers = passengers;
            Frequency = frequency;

            //DeadlineDate = текущее_время + Frequency;
        }
        
        public PassengerFlight(List<Place> placesList, EFrequencyType frequencyType, DateTime currentTime): 
            base(placesList, frequencyType, EFlightType.Passenger, currentTime)
        {
            Passengers = rnd.Next(MaxPassengers - MinPassengers) + MinPassengers;
            Frequency = (rnd.Next(MaxFrequency - MinFrequency) + MinFrequency) * 24 * 60;
            Income = rnd.Next(2, 5) * Distance;
        }

        public override bool IsAssignable(Plane plane)
        {
            return plane.MaxFlightDistance >= Distance && plane.PassengerCapacity >= Passengers;
        }

        public override void OnPlaneFlight(GameLogic gl)
        {
            if (FrequencyType == EFrequencyType.Regular) //Регулярный пассажирский рейс
            {
                PlannedFlightDate = PlannedFlightDate.AddMinutes(Frequency);

                if (gl.UserTime > DeadlineDate)
                {
                    gl.AddMoney(-1 * Penalty);
                    DeadlineDate = PlannedFlightDate;
                }
                else
                    DeadlineDate = DeadlineDate.AddMinutes(Frequency);
            }
            else
            {
                if (AssignedPlane == null)
                    return;
                gl.TakenFlights.Remove(this);
            }
        }

        public override void UpdateMoneyAfterFlight(GameLogic gameLogic) 
        {
            if (AssignedPlane == null)
            {
                return;
                //throw new Exception("No assigned plane!");
            }
            // Mean value of number of passengers - 0.7 * MaxCnt, Variance - 15, 
            // minNumber of passengers - 5
            int income = Income * MathExtension.Clamp(Convert.ToInt32(
                gameLogic.NormRandomGenerator.NextDouble() * 15 + 0.7 * Passengers), 
                5, Passengers);
            int expenditure = Convert.ToInt32(Place.CalcDistance(From, Where) * 
                GameLogic.FuelLiterCost * AssignedPlane.FuelPerKm);
            gameLogic.AddMoney(income - expenditure);
        }
    }
}