using System;

namespace AirportGame
{
    public enum EPlaneStatus
    {
        Flying,
        Idle
    }

    public enum EContractType
    {
        Purchase,
        Lease,
        Rent
    }

    public class PlaneContract
    {
        static public double SellCoef = 0.8;

        public EContractType Type { get; private set; }
        public long InitFee { get; private set; }
        public long DailyFee { get; private set; }
        public long Debt { get; private set; }
        public DateTime NextPay { get; private set; }

        public PlaneContract() {}

        public PlaneContract(long planeCost, Random rnd)
        {
            int tmp = rnd.Next(0, 3);
            if (tmp == 0)
            {
                Type = EContractType.Purchase;
                InitFee = planeCost;
                DailyFee = Debt = 0;
            } 
            else if (tmp == 1)
            {
                Type = EContractType.Lease;
                InitFee = (long)(planeCost * 0.2);
                DailyFee = (planeCost + 499) / 500;
                Debt = (long)(planeCost * 0.9);
            }
            else if (tmp == 2)
            {
                Type = EContractType.Rent;
                DailyFee = (planeCost + 999) / 1000;
                Debt = InitFee = 0;
            }
        }

        // Activate contract when User buys the plane
        public void ActivateContract(GameLogic gameLogic)
        {
            NextPay = gameLogic.UserTime.AddDays(1);
            if (Type == EContractType.Purchase || Type == EContractType.Lease)
            {
                gameLogic.AddMoney(-InitFee);
            }
            else if (Type == EContractType.Rent)
            {
                gameLogic.AddMoney(-DailyFee);
            }
        }

        // Deactivate contract when User sells/stop renting the plane
        public void DeactivateContract(GameLogic gameLogic, long planeCost)
        {
            NextPay = gameLogic.UserTime.AddDays(1);
            if (Type == EContractType.Rent)
            {
                gameLogic.AddMoney(-DailyFee);
            }
            else if (Type == EContractType.Lease)
            {
                gameLogic.AddMoney(-Math.Min(Debt, DailyFee));
            }
            else if (Type == EContractType.Purchase)
            {
                gameLogic.AddMoney((long)(planeCost * SellCoef));
            }
        }

        public void PayDaily(GameLogic gameLogic, long planeCost)
        {
            if (gameLogic.UserTime != NextPay)
                return;
            if (Type == EContractType.Lease)
            {
                long curFee = Math.Min(Debt, DailyFee);
                gameLogic.AddMoney(-curFee);
                Debt -= curFee;
                if (Debt == 0)
                {
                    Type = EContractType.Purchase;
                    InitFee = planeCost;
                    DailyFee = Debt = 0;
                }
            } 
            else if (Type == EContractType.Rent)
            {
                gameLogic.AddMoney(-DailyFee);
            }
        }
    }

    public class Plane
    {
        // Measured in tonnes
        public static double MinCarryingCapacity = 12;
        public static double MaxCarryingCapacity = 220;
        // Measured in people quantity
        public static int MinPassengerCapacity = 80;
        public static int MaxPassengerCapacity = 435;
        // Measured in kg per 1km
        public static double MinFuelPerKm = 1.8;
        public static double MaxFuelPerKm = 10.0;
        // Measured in km/h
        public static double MinMaxSpeed = 800;
        public static double MaxMaxSpeed = 1000;
        // Measured in kilometres
        public static double MinMaxFlightDistance = 2200;
        public static double MaxMaxFlightDistance = 12100;
        // Measured in rubles
        public static long MinPlaneCost = 50 * 1000 * 1000;
        public static long MaxPlaneCost = 300 * 1000 * 1000;

        public string Name { get; set; }
        public double CarryingCapacity { get; }
        public int PassengerCapacity { get; }
        public double MaxFlightDistance { get; }
        public double FuelPerKm { get; }
        public double MaxSpeed { get; }
        public int ServiceCost { get; }
        public EPlaneStatus Status { get; set; }
        public Place CurentPlace { get; }
        public long Cost { get; }
        public PlaneContract Contract { get; set; }
       
        public Plane()
        {}

        public Plane(string name, double carryingCapacity, int passengerCapacity, 
            double maxFlightDistance, double fuelPerKm, double maxSpeed, 
            int serviceCost, EPlaneStatus status, long cost, PlaneContract contract)
        {
            Name = name;
            CarryingCapacity = carryingCapacity;
            PassengerCapacity = passengerCapacity;
            MaxFlightDistance = maxFlightDistance;
            FuelPerKm = fuelPerKm;
            MaxSpeed = maxSpeed;
            ServiceCost = serviceCost;
            Status = status;
            CurentPlace = null;
            Cost = cost;
            Contract = contract;
        }
    }
}