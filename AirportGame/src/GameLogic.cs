﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace AirportGame
{
    public class GameLogic
    {
        readonly long WalletCapacity = (long)1E12;
        readonly int AirportCapacity = 10;
        readonly int ShopCapacity = 15;
        public delegate void EndGame();
        public Timer RealTime = new Timer();

        public List<Plane> Airport { get; private set; }
        public List<Place> Places { get; private set; }
        public List<AbstractFlight> TakenFlights { get;  set; }
        public List<AbstractFlight> AvailableFlights { get; private set; }
        public List<Plane> PlanesShop { get; private set; }
        
        public long Money { get; private set; }
        public DateTime UserTime { get; private set; }
        public event EndGame Bankruptcy;
        
        public Random NormRandomGenerator;
        static public double FuelLiterCost = 45;

        public GameLogic()
        {
            Places = new List<Place>();
            TakenFlights = new List<AbstractFlight>();

            PopulatePlaces();

            Airport = new List<Plane>();
            PlanesShop = ShopGeneration();

            Money = WalletCapacity / (long)1e3;

            RealTime.Interval = 1000;
            RealTime.Tick += new EventHandler(AddTime);
            UserTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 
                DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            RealTime.Start();
            NormRandomGenerator = new NormRandom();
            
            AvailableFlights = GetRandomFlights();
        }

        private void AddTime(Object sender, EventArgs e)
        {
            
            UserTime = UserTime.AddSeconds(60);
            
            if (UserTime.Hour == 0 && UserTime.Minute == 0)
                AvailableFlights = GetRandomFlights();


            //Итерируемся в обратном порядке, чтобы удаление элемента не вызвало исключения
            for (int i = (int)TakenFlights.Count - 1; i >= 0; i--)
            {
                var tflight = TakenFlights[i];

                // Пока полностью не реализован функционал полетов, может вести себя непредсказуемо
                if (tflight.PlannedFlightDate == UserTime && tflight.AssignedPlane != null)
                {
                    tflight.UpdateMoneyAfterFlight(this);
                    tflight.OnPlaneFlight(this);
                }
                
                if (tflight.DueDate == UserTime)
                {
                    // При проверке окончания действия рейса, выплачиваем неустойку только в том случае,
                    // если рейс разовый и ни разу не слетал
                    // Неустойка для регулярных рейсов сложнее и ее логика реализована в методе OnPlaneFlight
                    bool b = tflight is CargoFlight;
                    if (!b)
                        b = ((PassengerFlight) tflight).FrequencyType == EFrequencyType.OneTime;
                    if (b)
                        AddMoney(-1 * tflight.Penalty);
                    
                    // Если наступила дата окончания действия рейса - удаляем его
                    TakenFlights.Remove(tflight);
                }
            }

            //Смотрим самолёты и снимаем деньги за аренду и лизинг
            foreach (var plane in Airport)
            {
                plane.Contract.PayDaily(this, plane.Cost);
            }
        }

        /// <summary>
        /// Метод TimeAcceleration ускоряет пользовательское время
        /// </summary>
        /// <param name="rate">Коэффициент ускорения</param>
        public void TimeAcceleration(int rate)
        {
            RealTime.Interval = 1000 / rate;
        }

        /// <summary>
        /// Метод AddMoney добавляет деньги пользователю (может быть отрицательное)
        /// </summary>
        /// <param name="_money">Сумма прибыли</param>
        public void AddMoney(long _money)
        {
            if (_money > WalletCapacity || Money + _money > WalletCapacity)
            {
                Money = WalletCapacity;
                System.Windows.MessageBox.Show("Ваш кошелёк переполнен!", "Предупреждение", MessageBoxButton.OK);
                return;
            }
            if (_money + Money < 0) {
                Money = 0;
                System.Windows.MessageBox.Show("Ваш кошелёк пуст! Вы обанкротились!", "Проигрыш",
                    MessageBoxButton.OK);
                Bankruptcy();
                return;
            }
            Money += _money;
        }

        /// <summary>
        /// Метод ShopGeneration генерирует магазин самолётов
        /// </summary>
        public List<Plane> ShopGeneration()
        {
            List<string> jewishSurnames = ReadFromDataSet("DataSet JewishSurname.txt").ToList();
            Random rnd = new Random();
            List<Plane> planes = new List<Plane>();

            for (int i = 0; i < ShopCapacity; i++)
            {
                string name;
                do
                {
                    name = jewishSurnames[rnd.Next(0, jewishSurnames.Count - 1)] + "-" + 
                        rnd.Next(2, 100);
                }
                while (planes.Find(plane => plane.Name == name) != null);

                int interval = 100;
                double carryingCapacity = Plane.MinCarryingCapacity + rnd.Next(0, interval) * 
                    (Plane.MaxCarryingCapacity - Plane.MinCarryingCapacity) / interval;
                int passengerCapacity = 
                    rnd.Next(Plane.MinPassengerCapacity, Plane.MaxPassengerCapacity);
                double maxFlightDistance = Plane.MinMaxFlightDistance + rnd.Next(0, interval) * 
                    (Plane.MaxMaxFlightDistance - Plane.MinMaxFlightDistance) / interval;
                double fuelPerKm = Plane.MinFuelPerKm + rnd.Next(0, interval) * 
                    (Plane.MaxFuelPerKm - Plane.MinFuelPerKm) / interval;
                double maxSpeed = Plane.MinMaxSpeed + rnd.Next(0, interval) * 
                    (Plane.MaxMaxSpeed - Plane.MinMaxSpeed) / interval;
                int serviceCost = (int)(FuelLiterCost * fuelPerKm);
                EPlaneStatus status = (EPlaneStatus)rnd.Next(0, 1);
                long cost = (long)(((carryingCapacity - Plane.MinCarryingCapacity) / 
                       Math.Max(1, Plane.MaxCarryingCapacity - Plane.MinCarryingCapacity) +
                       ((double)passengerCapacity - Plane.MinPassengerCapacity) / 
                       Math.Max(1, Plane.MaxPassengerCapacity - Plane.MinPassengerCapacity) +
                       (maxFlightDistance - Plane.MinMaxFlightDistance) /
                       Math.Max(1, Plane.MaxMaxFlightDistance - Plane.MinMaxFlightDistance) +
                       rnd.Next(0, 1)) / 3 * Math.Max(1, Plane.MaxPlaneCost - Plane.MinPlaneCost) + 
                       Plane.MinPlaneCost) / (100 * 1000) * (100 * 1000);
                PlaneContract contract = new PlaneContract(cost, rnd);

                planes.Add(new Plane(name, carryingCapacity, passengerCapacity, maxFlightDistance, 
                    fuelPerKm, maxSpeed, serviceCost, status, cost, contract));
            }
            return planes;
        }

        public List<AbstractFlight> GetRandomFlights(int n=3)
        {
            var flights = new List<AbstractFlight>();
            var rnd = new Random();
            
            for (var i = 0; i < n; i++)
            {
                var flightType = (EFlightType) rnd.Next(2);
                AbstractFlight flight;

                if (flightType == EFlightType.Cargo)
                    flight = new CargoFlight(Places, UserTime);
                else
                {
                    var frequencyType = (EFrequencyType) rnd.Next(2);
                    flight = new PassengerFlight(Places, frequencyType, UserTime);
                }
                
                flights.Add(flight);
            }

            return flights;
        }

        private IEnumerable<string> ReadFromDataSet(string inputFile)
        {
            using (var inputStream = new StreamReader("..\\..\\resources\\" + inputFile))
            {
                string data;
                while ((data = inputStream.ReadLine()) != null)
                    yield return data;
            }
        }

        private void PopulatePlaces()
        {
            Places.Add(new Place("Москва", 0, 0));
            Places.Add(new Place("Анжи", 0, 3750));
            Places.Add(new Place("Нью Йорк", 2000, 0));
            Places.Add(new Place("Бобруйск", 0, 6000));
            Places.Add(new Place("Никита", 2500, 2500));
        }

        public void TakeFlight(AbstractFlight flight)
        {
            if (flight != null && AvailableFlights.Contains(flight))
            {
                if (flight is PassengerFlight && flight.FrequencyType == EFrequencyType.Regular)
                {
                    var passengerFlight = (PassengerFlight) flight;
                    passengerFlight.DeadlineDate = UserTime.AddSeconds(passengerFlight.Frequency);
                }

                TakenFlights.Add(flight);
                AvailableFlights.Remove(flight);
            }
        }

        public void DeleteFlight(AbstractFlight flight)
        {
            if (flight != null && TakenFlights.Contains(flight))
            {
                AvailableFlights.Add(flight);
                TakenFlights.Remove(flight);
                AddMoney(-1 * flight.Penalty);
            }
        }

        public void BuyPlane(Plane plane)
        {
            if (!PlanesShop.Contains(plane))
                return;
            if (Airport.Count == AirportCapacity)
            {
                System.Windows.MessageBox.Show("Невозможно купить самолёт. В вашем аэропорту больше нет места", "Предупреждение", MessageBoxButton.OK);
                return;
            }
            plane.Contract.ActivateContract(this);
            Airport.Add(plane);
            PlanesShop.Remove(plane);
        }

        public void SellPlane(Plane plane)
        {
            if (!Airport.Contains(plane))
                return;
            plane.Contract.DeactivateContract(this, plane.Cost);
            PlanesShop.Add(plane);
            Airport.Remove(plane);
        }
    }
}
