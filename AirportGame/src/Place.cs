using System;

namespace AirportGame
{
    public class Place
    {
        public string Name { get; }
        // Measured in kilometres
        private int x;
        private int y;

        public Place(string name, int x, int y)
        {
            Name = name;
            this.x = x;
            this.y = y;
        }

        public static int CalcDistance(Place placeX, Place placeY)
        {
            var tempX = (placeX.x - placeY.x) * (placeX.x - placeY.x);
            var tempY = (placeX.y - placeY.y) * (placeX.y - placeY.y);

            return (int)Math.Sqrt(tempX + tempY);
        }
    }
}